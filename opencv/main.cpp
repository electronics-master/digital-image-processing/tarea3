#include <opencv2/opencv.hpp>

//Used for interaction with the user
#include <boost/program_options.hpp>

#include "line.hpp"

// Standard Library
#include <fstream>
#include <iostream>
#include <string>

namespace po = boost::program_options;

int process(const std::string& input) {
    const cv::Vec3b color;
    cv::Mat frame;


    frame = cv::imread(input);

      if (!frame.empty()) {
			  cv::Point from(-50, -50);
			  cv::Point to(250, 250);
			  custom_line(frame, to, from, color);

			  std::string windowName = "Image with line";
			  cv::namedWindow(windowName);

			  cv::imshow(windowName, frame);
			  cv::waitKey(0);
      }
      else {
	std::cout << "Unable to open " << input  << std::endl;
      }

    return 0;
}

int main(int ac, char* av[]) {
    std::string input;

    try {
        po::options_description desc("Allowed Options");
        desc.add_options()
        ("help", "Produce help message")
        ("input", po::value<std::string>(), "Input image to process");

        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, desc), vm);
        po::notify(vm);

        if(vm.count("help") != 0u) {
            std::cout << desc << "\n";
            return 0;
        }

        if(vm.count("input") != 0u) {
            input = vm["input"].as<std::string>();
        } else {
            std::cout << "Input is required" << "\n";
            std::cout << desc << "\n";
            return 1;
        }

        return process(input);

    } catch(std::exception& e) {
        std::cerr << "error: " << e.what() << "\n";
        return 1;
    } catch(...) {
        std::cerr << "Exception of unknown type!\n";
    }

    return 0;

}
