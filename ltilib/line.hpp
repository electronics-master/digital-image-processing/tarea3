/**
 * Algorithm taken from: http://www.roguebasin.com/index.php?title=Bresenham%27s_Line_Algorithm
 */


#ifndef _LINE_HPP_
#define _LINE_HPP_

#include <ltilib/ltiMatrix.h>
#include <ltilib/ltiPoint.h>

static inline int check_limits(int val, int lower_limit, int upper_limit) {
  if ((val > lower_limit) && (val < upper_limit)) {
    return val;
  }
  else if (val < lower_limit) {
    return lower_limit;
  }
  else {
    return upper_limit;
  }
}

/**
 * Draw a line segment between two given points.
 * If one or two of the points lie outside the image then this function ...
 * @param img Image where the line is to be drawn
 * @param color Color of the line to be drawn
 * @param from Initial point of line segment
 * @param end Final point of line segment
 */
template<typename T>void
line(lti::matrix<T>& img, const T& color,const lti::ipoint& from,
     const lti::ipoint& to) {
    int x0 = from.x, y0 = from.y;
    int x1 = to.x, y1 = to.y;

    x0 = check_limits(x0, 0, img.columns()-1);
    x1 = check_limits(x1, 0, img.columns()-1);
    y0 = check_limits(y0, 0, img.rows()-1);
    y1 = check_limits(y1, 0, img.rows()-1);

    int delta_x(x1 - x0);
    // if x0 == x1, then it does not matter what we set here
    signed char const ix((delta_x > 0) - (delta_x < 0));
    delta_x = std::abs(delta_x) << 1;

    int delta_y(y1 - y0);
    // if y0 == y1, then it does not matter what we set here
    signed char const iy((delta_y > 0) - (delta_y < 0));
    delta_y = std::abs(delta_y) << 1;

    img.at(x0,y0) = color;

    if (delta_x >= delta_y) {
        // error may go below zero
        int error(delta_y - (delta_x >> 1));

        while (x0 != x1) {
            // reduce error, while taking into account the corner case of error == 0
            if ((error > 0) || (!error && (ix > 0))) {
                error -= delta_x;
                y0 += iy;
            }
            // else do nothing

            error += delta_y;
            x0 += ix;

            img.at(x0,y0) = color;
        }
    } else {
        // error may go below zero
        int error(delta_x - (delta_y >> 1));

        while (y0 != y1) {
            // reduce error, while taking into account the corner case of error == 0
            if ((error > 0) || (!error && (iy > 0))) {
                error -= delta_y;
                x0 += ix;
            }
            // else do nothing

            error += delta_x;
            y0 += iy;

            img.at(x0,y0) = color;
        }
    }
}

#endif /* _LINE_HPP_ */
